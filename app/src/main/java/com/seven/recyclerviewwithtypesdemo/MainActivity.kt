package com.seven.recyclerviewwithtypesdemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.seven.recyclerviewwithtypesdemo.adaptors.ItemAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

            // Set the LayoutManager that this RecyclerView will use.
            recycler_view_items.layoutManager = LinearLayoutManager(this)

            // Adapter class is initialized and list is passed in the param.
            val itemAdapter = ItemAdapter(this, getItemsList())

            // adapter instance is set to the recyclerview to inflate the items.
            recycler_view_items.adapter = itemAdapter
        }

    private fun getItemsList(): ArrayList<DataModel> {
        val list = ArrayList<DataModel>()

        for(i in 1..15){
            if(i%2 == 1)
                list.add(DataModel("Roll No $i", ItemAdapter.GIRL))
            else
                list.add(DataModel("Roll No $i", ItemAdapter.BOY))
        }
        return list
    }
}