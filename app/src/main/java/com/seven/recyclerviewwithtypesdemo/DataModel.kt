package com.seven.recyclerviewwithtypesdemo

data class DataModel(val itemName: String, val viewType: Int)